import os, sys
from evaluation_system.api import plugin, parameters
from evaluation_system.misc import config 

class TINT(plugin.PluginAbstract):
    __category__ = 'statistical'
    __tags__ = ['Tracking', 'precipitation']
    tool_developer = {'name':'Etor Lucio-Eceiza', 'email':'lucio-eceiza@dkrz.de'}
    ## tool_developer = {'name':['Etor Lucio-Eceiza', 'Mahesh Ramadoss'], 'email':['lucio-eceiza@dkrz.de', 'ramadoss@drkz.de']}
    __short_description__  = "Tracking and Object-Based Analysis of Clouds (tobac)] that will evolve into a sustainable choice to analyze atmospheric features."
    __long_description__   = """
    the algorithm’s ability to study convective life cycles using radar and simulated data over Darwin, Australia. The algorithm skillfully tracks individual convective cells (a few pixels in size) and large convective systems. The duration of tracks and cell size are found to be lognormally distributed over Darwin. The evolution of size and precipitation types of isolated convective cells is presented in the Lagrangian perspective. This algorithm is part of a vision for a modular platform [viz., TINT is not TITAN (TINT) and Tracking and Object-Based Analysis of Clouds (tobac)] that will evolve into a sustainable choice to analyze atmospheric features.
    """
    __version__ = (1,0,0)
    __parameters__ = parameters.ParameterDictionary(
                        parameters.File(name='input', max_items=1, item_separator=',',mandatory=True, help='NetCDF file to be plotted E.g. -> /path/2/datafile.nc'),
                        parameters.Directory(name='outputdir', default='$USER_OUTPUT_DIR/$SYSTEM_DATETIME', mandatory=True, help='The default output directory of XCES'),
                        parameters.Bool(name='cacheclear', default=True, help='Option switch to NOT clear the cache'),
                        parameters.CacheDirectory(name='cachedir', default='$USER_CACHE_DIR', help='Temporal directory for internal calculations'),
                        parameters.String(name='title', help='Option to choose a title for the plot'),
                        parameters.String(name='seldate', mandatory=True, help='Option to choose timerange start,end via YYYY-MM-DD,YYYY-MM-DD.'),
                        parameters.String(name='latlon', mandatory=True, default='30,80,-20,30', help='Option to choose an area in file. Usage: startlat,endlat,startlon,endlon -> For Europe 30,80,-20,30'),
                        parameters.Bool(name='dryrun', default=True, help='Set "True" for just showing the result of pre-processing. Set "False" to process data.'),
                    )

    
    def runTool(self, config_dict=None):
        ## input         = config_dict['input']
        input        = config_dict['input']
        outputdir      = config_dict['outputdir']
        cacheclear     = config_dict['cacheclear']
        cachedir       = config_dict['cachedir']
        title          = config_dict['title']
        seldate        = config_dict['seldate']
        latlon         = config_dict['latlon']
        dryrun         = config_dict['dryrun']


        errorfile=cachedir+"/error.txt"
        if not os.path.exists(cachedir):
            os.makedirs(cachedir)
        else:
            print "\nBE CAREFUL! Directory %s already exists." % cachedir


        if not os.path.exists(outputdir):
            os.makedirs(outputdir)
        else:
            print "\nBE CAREFUL! Directory %s already exists." % outputdir


        ## result= self.call('bash %s/wetdry_script.sh %s %s %s %s %s %s %s %s %s %s' % (self.getClassBaseDir(),input,outputdir,cacheclear,cachedir,latlon,seldate,title,dryrun,link2database,errorfile)) 
        result= self.call('%s/tint.py %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (self.getClassBaseDir(), input, outputdir, cacheclear, cachedir, latlon, seldate, title, dryrun, errorfile)) 
        

        if link2database:
            self.linkmydata(os.path.join(outputdir,'CMOR'))


        return self.prepareOutput(config_dict['outputdir']) 

       
